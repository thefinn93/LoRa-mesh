#include <Arduino.h>

#include "utils.h"
#include "lora.h"

// Pin for monitoring battery voltage
#define BATTERY_PIN 35

RTC_DATA_ATTR uint16_t low_voltage = 0;
RTC_DATA_ATTR uint16_t high_voltage = 0;

#define RTC_TABLE_SIZE 100

RTC_DATA_ATTR uint8_t neighborTableSize = 0;
RTC_DATA_ATTR NeighborTableEntry neighborTable[RTC_TABLE_SIZE];

RTC_DATA_ATTR uint8_t routingTableSize = 0;
RTC_DATA_ATTR RoutingTableEntry routingTable[RTC_TABLE_SIZE];

RTC_DATA_ATTR uint8_t messageCount = 0;

float getVoltage() {
#ifdef DISABLE_VOLTAGE_CHECKING
  return 4.5;
#else
  return (float)(analogRead(BATTERY_PIN)) / 4095*2*3.3*1.1;
#endif
}

void recordVoltage() {
  uint16_t v = getVoltage()*1000;
  if(v < low_voltage || low_voltage == 0) {
    low_voltage = v;
  }

  if(v > high_voltage) {
    high_voltage = v;
  }
}

char nodeAddress[ADDR_LENGTH*2 + 1] = {'\0'};
void setupNodeAddress() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  sprintf(nodeAddress, PRINTF_ADDRESS_PATTERN, mac[2], mac[3], mac[4], mac[5]);
}

void setLoraAddress() {
  LL2->setLocalAddress(nodeAddress);
}

String getNodeAddress() {
  return String(nodeAddress);
}

void loadNetworkState() {
  LL2->loadRouteTable(routingTable, routingTableSize);
  LL2->loadNeighborTable(neighborTable, neighborTableSize);
  LL2->setMessageCount(messageCount);
  LOG("loaded %i routing table entries and %i neighbor table entries", routingTableSize, neighborTableSize);
}

void saveNetworkState() {
  routingTableSize = LL2->saveRouteTable(routingTable, RTC_TABLE_SIZE);
  neighborTableSize = LL2->saveNeighborTable(neighborTable, RTC_TABLE_SIZE);
  messageCount = LL2->messageCount();
  LOG("saved %i routing table entries and %i neighbor table entries", routingTableSize, neighborTableSize);
}

void meshPromMetrics(AsyncResponseStream* response) {
  saveNetworkState();

  response->print(F("# HELP neighbor_last_received\n"));
  response->print(F("# TYPE neighbor_last_received gauge\n"));
  for(int i = 0; i < neighborTableSize; i++) {
    response->printf("neighbor_last_received{neighbor_address=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(neighborTable[i].address), neighborTable[i].lastReceived);
  }
  response->print(F("# HELP neighbor_packet_success\n"));
  response->print(F("# TYPE neighbor_packet_success gauge\n"));
  for(int i = 0; i < neighborTableSize; i++) {
    response->printf("neighbor_packet_success{neighbor_address=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(neighborTable[i].address), neighborTable[i].packet_success);
  }
  response->print(F("# HELP neighbor_metric\n"));
  response->print(F("# TYPE neighbor_metric gauge\n"));
  for(int i = 0; i < neighborTableSize; i++) {
    response->printf("neighbor_metric{neighbor_address=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(neighborTable[i].address), neighborTable[i].metric);
  }
  response->print(F("# HELP neighbor_table_size\n"));
  response->print(F("# TYPE neighbor_table_size gauge\n"));
  response->printf("neighbor_table_size{node_id=\"%s\"} %i\n", nodeAddress, neighborTableSize);


  response->print(F("# HELP route_distance\n"));
  response->print(F("# TYPE route_distance gauge\n"));
  for(int i = 0; i < routingTableSize; i++) {
    response->printf("route_distance{destination=\"" PRINTF_ADDRESS_PATTERN "\", next_hop=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(routingTable[i].destination), PRINTF_ADDRESS(routingTable[i].nextHop), routingTable[i].distance);
  }
  response->print(F("# HELP route_last_received\n"));
  response->print(F("# TYPE route_last_received gauge\n"));
  for(int i = 0; i < routingTableSize; i++) {
    response->printf("route_last_received{destination=\"" PRINTF_ADDRESS_PATTERN "\", next_hop=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(routingTable[i].destination), PRINTF_ADDRESS(routingTable[i].nextHop), routingTable[i].lastReceived);
  }
  response->print(F("# HELP route_metric\n"));
  response->print(F("# TYPE route_metric gauge\n"));
  for(int i = 0; i < routingTableSize; i++) {
    response->printf("route_metric{destination=\"" PRINTF_ADDRESS_PATTERN "\", next_hop=\"" PRINTF_ADDRESS_PATTERN "\"} %i\n", PRINTF_ADDRESS(routingTable[i].destination), PRINTF_ADDRESS(routingTable[i].nextHop), routingTable[i].metric);
  }
  response->print(F("# HELP routing_table_size\n"));
  response->print(F("# TYPE routing_table_size gauge\n"));
  response->printf("routing_table_size{node_id=\"%s\"} %i\n", nodeAddress, routingTableSize);

  response->print(F("# HELP current_sequence_number\n"));
  response->print(F("# TYPE current_sequence_number gauge\n"));
  response->printf("current_sequence_number{node_id=\"%s\"} %i\n", nodeAddress, messageCount);
}
