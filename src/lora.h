#ifndef LORA_MESH_LORA_H
#define LORA_MESH_LORA_H
#include <Layer1_LoRa.h>
#include <LoRaLayer2.h>

#define DATAGRAM_HEADER 5

extern Layer1Class *Layer1;
extern LL2Class *LL2;
#endif
