#include <Arduino.h>
#include <Layer1_LoRa.h>
#include <LoRaLayer2.h>

#include "cli.h"
#include "config.h"
#include "utils.h"
#include "wifi.h"


void printWiFi() {
  switch(WiFi.status()) {
    case WL_CONNECTED:
      Serial.println(F("wifi status: WL_CONNECTED"));
    break;
    case WL_NO_SHIELD:
      Serial.println(F("wifi status: WL_NO_SHIELD"));
    break;
    case WL_IDLE_STATUS:
      Serial.println(F("wifi status: WL_IDLE_STATUS"));
    break;
    case WL_NO_SSID_AVAIL:
      Serial.println(F("wifi status: WL_NO_SSID_AVAIL"));
    break;
    case WL_SCAN_COMPLETED:
      Serial.println(F("wifi status: WL_SCAN_COMPLETED"));
    break;
    case WL_CONNECT_FAILED:
      Serial.println(F("wifi status: WL_CONNECT_FAILED"));
    break;
    case WL_CONNECTION_LOST:
      Serial.println(F("wifi status: WL_CONNECTION_LOST"));
    break;
    case WL_DISCONNECTED:
      Serial.println(F("wifi status: WL_DISCONNECTED"));
    break;
  }

  switch(WiFi.getMode()) {
    case WIFI_MODE_NULL:
      Serial.println(F("wifi mode: WIFI_MODE_NULL (usually means wifi is off)"));
    break;
    case WIFI_AP:
      Serial.println(F("wifi mode: WIFI_AP"));
    break;
    case WIFI_STA:
      Serial.println(F("wifi mode: WIFI_STA (client)"));
    break;
    case WIFI_MODE_APSTA:
      Serial.println(F("wifi mode: WIFI_MODE_APSTA (wtf is that!?)"));
    break;
    case WIFI_MODE_MAX:
      Serial.println(F("wifi mode: WIFI_MODE_MAX (wtf is that!?)"));
    break;
  }
}

void cliInputCB(String buffer, LL2Class *LL2) {
  if(buffer == "routing") {
    char r_table[256] = {'\0'}; //TODO: need to check size of routing table to allocate correct amount of memory
    LL2->getRoutingTable(r_table);
    Serial.printf("%s", r_table);
    Serial.flush();
    return;
  }
  if(buffer == "voltage") {
    Serial.printf("%.3f", getVoltage());
    Serial.println();
    return;
  }
  if(buffer == "config") {
    printConfig();
    return;
  }
  if(buffer == "wifi") {
    printWiFi();
    return;
  }
  Serial.printf("Ignoring unknown command: %s", buffer.c_str());
  Serial.println();
}
