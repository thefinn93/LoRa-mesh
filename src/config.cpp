#include <nvs_flash.h>
#include <pb_encode.h>
#include <pb_decode.h>

#include "config.h"
#include "utils.h"
#include "proto/config.pb.h"
// #include "proto/messages.pb.h"

#define CONFIG_NAMESPACE "lm"
#define CONFIG_KEY CONFIG_NAMESPACE

Preferences preferences;

void ensureConfigIsSafe() {
  if(config.wifi_disable_time < 60) {
    config.wifi_disable_time = 60;
  }

  if(config.beacon_interval < 30) {
    config.beacon_interval = 30;
  }

  if(config.router_ping_interval < 30) {
    config.router_ping_interval = 30;
  }
}

void initConfig() {
  esp_err_t err_init = nvs_flash_init();
  if (err_init == ESP_ERR_NVS_NO_FREE_PAGES || err_init == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    // NVS partition was truncated and needs to be erased
    // Retry nvs_flash_init
    ESP_ERROR_CHECK(nvs_flash_erase());
    err_init = nvs_flash_init();
  }
  ESP_ERROR_CHECK(err_init);
  if(!preferences.begin(CONFIG_NAMESPACE, false)) {
    LOG("error opening preferences");
    return;
  }

  size_t size = preferences.getBytesLength(CONFIG_KEY);
  uint8_t buffer[size];
  preferences.getBytes(CONFIG_KEY, &buffer, size);

  pb_istream_t stream = pb_istream_from_buffer(buffer, size);
  bool status = pb_decode(&stream, Configuration_fields, &config);
  ensureConfigIsSafe();
  if (!status) {
    LOG("Decoding configuration failed: %s", PB_GET_ERROR(&stream));
    return;
  }
  preferences.end();
  LOG(" configuration loaded");
}

void saveConfig() {
  uint8_t buffer[Configuration_size];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, Configuration_size);
  boolean status = pb_encode(&stream, Configuration_fields, &config);
  if (!status) {
      LOG("[%lu] configuration encoding failed: %s\n", time(NULL), PB_GET_ERROR(&stream));
      return;
  }

  preferences.begin(CONFIG_NAMESPACE, false);
  LOG("Writing config");
  printConfig();
  preferences.putBytes(CONFIG_KEY, &buffer, stream.bytes_written);
  preferences.end();
}

void printConfig() {
  LOG("Configuration: ");
  LOG(" %s: %i", CONFIG_KEY_BEACON_INTERVAL, config.beacon_interval);
  LOG(" %s: %i", CONFIG_KEY_ROUTER_PING_INTERVAL, config.router_ping_interval);
  LOG(" %s: %i", CONFIG_KEY_WIFI_DISABLE_TIME, config.wifi_disable_time);
  LOG(" %s: %i", CONFIG_KEY_WIFI_CLIENT, config.wifi_client);
  LOG(" %s: %s", CONFIG_KEY_WIFI_SSID, config.wifi_ssid);
  LOG(" %s: [REDACTED]", CONFIG_KEY_WIFI_PSK);
}

Configuration config = Configuration_init_zero;
