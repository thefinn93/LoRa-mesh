#ifndef LORA_MESH_WIFI_H
#define LORA_MESH_WIFI_H

void setupWifi();
void wifiLoop();
void reactivateAP();
bool wifiEnabled();
void printWiFiStatus();
void printWiFi();

#endif
