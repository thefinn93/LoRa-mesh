#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include <ArduinoJson.h>
#include <pb_encode.h>

#include "config.h"
#include "utils.h"
#include "web.h"
#include "lora.h"

AsyncWebServer server(80);
AsyncEventSource events("/events");

void configStringFromJson(char dest[40], JsonVariant source) {
  String sourceStr = source.as<String>();
  int size = sourceStr.length();
  for(int i = 0; i < 40; i++) {
    if(i < size) {
      dest[i] = sourceStr[i];
    } else {
      dest[i] = '\0';
    }
  }
}

void setupWebserver() {
  // Setup event source
  server.addHandler(&events);
  events.onConnect([](AsyncEventSourceClient *client) {
    DynamicJsonDocument json(JSON_OBJECT_SIZE(3));
    json["node_id"] = getNodeAddress();
    String output;
    serializeJson(json, output);
    client->send(output.c_str(), "hello");
  });

  server.onNotFound([](AsyncWebServerRequest *request) {
    String ip;
    if(ON_STA_FILTER(request)) {
      ip = WiFi.localIP().toString();
    } else {
      ip = WiFi.softAPIP().toString();
    }

    if(ip == request->host()) {
      request->send(404, "text/html", "[clever 404 page not found]");
    } else {
      request->redirect("http://" + ip);
    }
  });

  server.on("/config.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream* response = request->beginResponseStream("application/json");
    DynamicJsonDocument json(JSON_OBJECT_SIZE(6));
    json[CONFIG_KEY_BEACON_INTERVAL] = config.beacon_interval;
    json[CONFIG_KEY_ROUTER_PING_INTERVAL] = config.router_ping_interval;
    json[CONFIG_KEY_WIFI_DISABLE_TIME] = config.wifi_disable_time;
    json[CONFIG_KEY_WIFI_CLIENT] = config.wifi_client;
    json[CONFIG_KEY_WIFI_SSID] = config.wifi_ssid;
    serializeJsonPretty(json, *response);
    request->send(response);
  });

  server.on("/broadcast_upgrade_packet", HTTP_GET, [](AsyncWebServerRequest *request) {
    int bytesSent = -1;
    struct Datagram d;
    memcpy(d.destination, BROADCAST, ADDR_LENGTH);
    d.type = 'u';

    DiagnosticMessage message = DiagnosticMessage_init_zero;
    message.type = DiagnosticMessage_Type_OTA_UPGRADE;
    pb_ostream_t stream = pb_ostream_from_buffer(d.message, sizeof(d.message));
    boolean status = pb_encode(&stream, DiagnosticMessage_fields, &message);
    if(!status) {
        LOG("stats encoding failed: %s\n", PB_GET_ERROR(&stream));
        return;
    } else {
      bytesSent = LL2->writeData(d, stream.bytes_written + DATAGRAM_HEADER);
      LOG("Sent %u byte message got result: %i", stream.bytes_written + DATAGRAM_HEADER, bytesSent);
    }

    AsyncResponseStream* response = request->beginResponseStream("application/json");
    DynamicJsonDocument json(JSON_OBJECT_SIZE(6));
    json["status"] = status;
    json["bytesSent"] = bytesSent;
    serializeJsonPretty(json, *response);
    request->send(response);
  });

  server.addHandler(new AsyncCallbackJsonWebHandler("/config.json", [](AsyncWebServerRequest *request, JsonVariant &j) {
    JsonObject json = j.as<JsonObject>();

    if(json.containsKey(CONFIG_KEY_BEACON_INTERVAL)) {
      config.beacon_interval = json[CONFIG_KEY_BEACON_INTERVAL];
    }

    if(json.containsKey(CONFIG_KEY_ROUTER_PING_INTERVAL)) {
      config.router_ping_interval = json[CONFIG_KEY_ROUTER_PING_INTERVAL];
    }

    if(json.containsKey(CONFIG_KEY_WIFI_DISABLE_TIME)) {
      config.wifi_disable_time = json[CONFIG_KEY_WIFI_DISABLE_TIME];
    }

    if(json.containsKey(CONFIG_KEY_WIFI_CLIENT)) {
      config.wifi_client = json[CONFIG_KEY_WIFI_CLIENT];
    }

    if(json.containsKey(CONFIG_KEY_WIFI_SSID)) {
      configStringFromJson(config.wifi_ssid, json[CONFIG_KEY_WIFI_SSID]);
    }

    if(json.containsKey(CONFIG_KEY_WIFI_PSK)) {
      configStringFromJson(config.wifi_psk, json[CONFIG_KEY_WIFI_PSK]);
    }

    saveConfig();
    AsyncJsonResponse * response = new AsyncJsonResponse();
    response->getRoot()[F("success")] = true;
    response->setLength();
    request->send(response);
    delay(100);
    ESP.restart();
  }));

  server.on("/metrics", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream* response = request->beginResponseStream("text/plain; version>=0.4.0");

    response->print(F("# HELP wifi_rssi_dbm Received Signal Strength Indication (dBm)\n"));
    response->print(F("# TYPE wifi_rssi_dbm counter\n"));
    response->printf("wifi_rssi_dbm{} %d\n\n", WiFi.RSSI());

    response->print(F("# HELP heap_free_b Amount of heap free (bytes)\n"));
    response->print(F("# TYPE heap_free_b gauge\n"));
    response->printf("heap_free_b{} %d\n\n", ESP.getFreeHeap());

    response->print(F("# HELP uptime_ms Uptime in milliseconds\n"));
    response->print(F("# TYPE uptime_ms gauge\n"));
    response->printf("uptime_ms{} %lu\n\n", millis());

    meshPromMetrics(response);

    request->send(response);
  });
  server.begin();
}

void announceIncomingPacket(StatusMessage message, uint8_t sender[ADDR_LENGTH]) {
  DynamicJsonDocument json(JSON_OBJECT_SIZE(6));
  char senderAddr[6];
  sprintf(senderAddr, "%x%x%x%x", sender[0], sender[1], sender[2], sender[3]);
  json["sender"] = senderAddr;
  json["time"] = message.time;
  json["voltage"] = message.voltage;
  json["voltage_low"] = message.voltage_low;
  json["voltage_high"] = message.voltage_high;
  String output;
  serializeJson(json, output);
  events.send(output.c_str(), "lora_receive");
}
