#include <ArduinoOTA.h>
#include <ESPmDNS.h>
#include <WiFi.h>

#include "wifi.h"
#include "utils.h"
#include "config.h"
#include "web.h"

// How many times to try to connect to the AP before giving up
#define WIFI_CONNECT_ATTEMPTS 10

unsigned long wifi_timer = 0;

bool connectToAP() {
  LOG("Connecting to SSID: %s", config.wifi_ssid);

  if(strcmp("", config.wifi_psk) == 0) {
    WiFi.begin(config.wifi_ssid);
  } else {
    WiFi.begin(config.wifi_ssid, config.wifi_psk);
  }

  int status = WiFi.status();

  while(status != WL_CONNECTED && status != WL_DISCONNECTED) {
    delay(500);
    status = WiFi.status();
  }

  return status == WL_CONNECTED;
}

void setupWifi() {
  bool connected = false;
  if(config.wifi_client) {
    WiFi.mode(WIFI_STA);
    for(int i = 0; i < WIFI_CONNECT_ATTEMPTS; i++) {
      connected = connectToAP();
      if(connected) {
        break;
      }
      LOG("connect attempt %i/%i failed, sleeping %i seconds", i, WIFI_CONNECT_ATTEMPTS, i);
      delay(1000*i); // linear backoff is good enough for now
    }

    if(connected) {
      LOG("successfully connected to wifi network. IP address: %s", String(WiFi.localIP()).c_str());
    } else {
      LOG("failed to connect to wifi");
    }
  }
  String hostname = String("lora-mesh-") + getNodeAddress();
  if(!connected) {
    WiFi.mode(WIFI_AP);
    WiFi.softAP(hostname.c_str());
    LOG("wifi AP mode broadcasting SSID %s", hostname.c_str());
    wifi_timer = time(NULL) + config.wifi_disable_time;
  }
  WiFi.setHostname(hostname.c_str());

  if(MDNS.begin(hostname.c_str())) {
    MDNS.addService("_prometheus-http", "tcp", 80);
  } else {
    LOG("Error setting up MDNS responder!");
  }

  ArduinoOTA.setHostname(hostname.c_str());
  ArduinoOTA.begin();
  setupWebserver();
}

void wifiLoop() {
  if(config.wifi_client) {
    ArduinoOTA.handle();
    return;
  }

  if(WiFi.getMode() != WIFI_MODE_NULL) {
    ArduinoOTA.handle();

#ifdef ENABLE_POWER_OPTIMIZATIONS
    if(wifi_timer < time(NULL)) {
      LOG("Disabling wifi to preserve battery");
      WiFi.disconnect();
      WiFi.mode(WIFI_MODE_NULL);
    }
#endif
  } else if(wifi_timer > time(NULL)) {
    setupWifi();
  }
}

void reactivateAP() {
  wifi_timer = time(NULL) + config.wifi_disable_time;
}

bool wifiEnabled() {
  return WiFi.getMode() != WIFI_MODE_NULL || time(NULL) < wifi_timer;
}
