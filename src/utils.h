#ifndef LORA_MESH_UTILS_H
#define LORA_MESH_UTILS_H
#include <LoRaLayer2.h>
#include <ESPAsyncWebServer.h>

#define PRINTF_ADDRESS(addr) addr[0], addr[1], addr[2], addr[3]
#define PRINTF_ADDRESS_PATTERN "%02x%02x%02x%02x"

#ifdef LOG_TO_SERIAL
#define LOG(message, ...) Serial.printf("[%lu] " message, time(NULL), ##__VA_ARGS__); Serial.println();
#else
#define LOG(message, ...)
#endif


extern uint16_t low_voltage;
extern uint16_t high_voltage;

float getVoltage();
void recordVoltage();

String getNodeAddress();
void setupNodeAddress();
void setLoraAddress();

void saveNetworkState();
void loadNetworkState();

void meshPromMetrics(AsyncResponseStream*);

#endif
