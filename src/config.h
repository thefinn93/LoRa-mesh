#ifndef LORA_MESH_CONFIG_H
#define LORA_MESH_CONFIG_H
#include <Preferences.h>

#include "proto/config.pb.h"
#include "proto/messages.pb.h"

#define CONFIG_KEY_BEACON_INTERVAL "beacon_interval"
#define CONFIG_KEY_ROUTER_PING_INTERVAL "router_ping_interval"
#define CONFIG_KEY_WIFI_DISABLE_TIME "wifi_disable_time"
#define CONFIG_KEY_WIFI_CLIENT "wifi_client"
#define CONFIG_KEY_WIFI_SSID "wifi_ssid"
#define CONFIG_KEY_WIFI_PSK "wifi_psk"

extern Configuration config;

void initConfig();
void saveConfig();
void printConfig();

#endif
