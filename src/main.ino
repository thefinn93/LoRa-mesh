#include <Arduino.h>
#include <stdio.h>
#include <LoRaLayer2.h>
#include <ArduinoOTA.h>
#include <pb_encode.h>
#include <pb_decode.h>
#include <esp_wifi.h>
#include <esp_bt.h>
#include <time.h>

#include "cli.h"
#include "config.h"
#include "proto/messages.pb.h"
#include "utils.h"
#include "web.h"
#include "wifi.h"
#include "lora.h"

// MIN_WIFI_VOLTAGE is the voltage to disable wifi entirely at boot (to perserve battery)
#define MIN_WIFI_VOLTAGE 3.6

// Time between low battery checks (seconds)
#define LOW_BATTERY_SLEEP_INTERVAL 1800 // 30 minutes

// The battery voltage at which deep sleep will be entered
#define LOW_BATTERY_SLEEP_VOLTAGE 3.15

boolean upgrading = false;

#ifdef ENABLE_POWER_OPTIMIZATIONS
esp_sleep_wakeup_cause_t wakeup_reason = esp_sleep_get_wakeup_cause();
bool handle_wake_for_dio0_rise = false;

RTC_DATA_ATTR unsigned long nextRouterPingInterval = 10;
#endif

#ifdef SEND_BEACONS
RTC_DATA_ATTR unsigned long nextBeacon = 0;
#endif


// TODO: also implement this in the ULP coprocessor so the high power CPU doesn't ever need to wake up during low-power situations
// see: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/ulp.html
// outside contributions welcome
void lowVoltageSleep() {
  if(getVoltage() > LOW_BATTERY_SLEEP_VOLTAGE) {
    return;
  }

  btStop();
  esp_wifi_stop();
  esp_bt_controller_disable();

  esp_sleep_enable_timer_wakeup(LOW_BATTERY_SLEEP_INTERVAL);
  esp_deep_sleep_start();
}

void setup() {
#ifdef ENABLE_POWER_OPTIMIZATIONS
  lowVoltageSleep();
  setCpuFrequencyMhz(80);
#endif

  Serial.begin(115200);
  initConfig();
  setupNodeAddress();

  LOG("Node %s booting, input voltage is %0.3fv", getNodeAddress().c_str(), getVoltage());


#ifdef ENABLE_POWER_OPTIMIZATIONS
  switch(wakeup_reason) {
    case ESP_SLEEP_WAKEUP_UNDEFINED:
      LOG("wakeup reason: undefined");
      break;
    case ESP_SLEEP_WAKEUP_ALL:
      LOG("wakeup reason: all");
      break;
    case ESP_SLEEP_WAKEUP_EXT0:
      handle_wake_for_dio0_rise = true;
      LOG("wakeup reason: ext0");
      break;
    case ESP_SLEEP_WAKEUP_EXT1:
      LOG("wakeup reason: ext1");
      break;
    case ESP_SLEEP_WAKEUP_TIMER:
      LOG("wakeup reason: timer");
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
      LOG("wakeup reason: touchpad");
      break;
    case ESP_SLEEP_WAKEUP_ULP:
      LOG("wakeup reason: ulp");
      break;
    case ESP_SLEEP_WAKEUP_GPIO:
      LOG("wakeup reason: gpio");
      break;
    case ESP_SLEEP_WAKEUP_UART:
      LOG("wakeup reason: uart");
      break;
  }
  if(getVoltage() > MIN_WIFI_VOLTAGE && wakeup_reason == ESP_SLEEP_WAKEUP_UNDEFINED) {setupWifi();}
#else
  setupWifi();
#endif

  Layer1 = new Layer1Class();
  LoRa.enableCrc();
  Layer1->setPins(LORA_CS, LORA_RST, LORA_IRQ);
#ifdef ENABLE_POWER_OPTIMIZATIONS
  bool wokenFromSleep = wakeup_reason != ESP_SLEEP_WAKEUP_UNDEFINED;
#else
  bool wokenFromSleep = false;
#endif
  LOG("Initializing layer1 (initialize LoRa device? %s)", wokenFromSleep ? "no" : "yes");
  if(!Layer1->init(!wokenFromSleep)) {
    LOG("Failed to initialize LoRa! Sleeping for 60 seconds");
    esp_sleep_enable_timer_wakeup(60000000);
    esp_deep_sleep_start();
  }
  LOG("Initializing layer2");
  LL2 = new LL2Class(Layer1); // initialize Layer2
  setLoraAddress();
#ifdef ENABLE_POWER_OPTIMIZATIONS
  LL2->setInterval(0);
#else
  LL2->setInterval(config.router_ping_interval*1000);
#endif


#ifdef ENABLE_POWER_OPTIMIZATIONS
  // Only run OTA listener and web server when not waking from deep sleep
  if(wakeup_reason != ESP_SLEEP_WAKEUP_UNDEFINED) {
    loadNetworkState();
    return;
  }
#endif

  ArduinoOTA.onStart([]() {
    LoRa.sleep();
    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    LOG("Start updating %s", ArduinoOTA.getCommand() == U_FLASH ? "sketch" : "filesystem");
    upgrading = true;
  })
  .onEnd([]() {
    LOG("Update end");
    upgrading = false;
  })
#ifdef LOG_TO_SERIAL
  .onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("[%lu] Update progress: %u%% (%u/%u)\r", time(NULL), (progress / (total / 100)), progress, total);
    Serial.flush();
  })
#endif
  .onError([](ota_error_t error) {
    switch(error) {
      case OTA_AUTH_ERROR:
        LOG("Update error: auth failed");
      break;
      case OTA_BEGIN_ERROR:
        LOG("Update error: begin failed");
      break;
      case OTA_CONNECT_ERROR:
        LOG("Update error: connect failed");
      break;
      case OTA_RECEIVE_ERROR:
        LOG("Update error: receive failed");
      break;
      case OTA_END_ERROR:
        LOG("Update error: end failed");
      break;
    }
    upgrading = false;
  });

}

void sendStatusMessage() {
  Datagram datagram = {0xff, 0xff, 0xff, 0xff, 'z'};
  StatusMessage message = StatusMessage_init_zero;
  message.time = time(NULL);
  message.voltage = getVoltage();
  message.voltage_low = (float)low_voltage/(float)1000;
  message.voltage_high = (float)high_voltage/(float)1000;

  pb_ostream_t stream = pb_ostream_from_buffer(datagram.message, sizeof(datagram.message));
  boolean status = pb_encode(&stream, StatusMessage_fields, &message);
  if (!status) {
      LOG("stats encoding failed: %s\n", PB_GET_ERROR(&stream));
      return;
  }
  int ret = LL2->writeData(datagram, stream.bytes_written + DATAGRAM_HEADER);
  LOG("Sent %u byte message got result: %i", stream.bytes_written, ret);
}

void printStatusMessage(Packet packet) {
  StatusMessage message = StatusMessage_init_zero;
  pb_istream_t stream = pb_istream_from_buffer(packet.datagram.message, packet.totalLength - HEADER_LENGTH - DATAGRAM_HEADER);
  boolean status = pb_decode(&stream, StatusMessage_fields, &message);
  if(!status) {
    LOG("message decode failure: %s\n", PB_GET_ERROR(&stream));
    return;
  }
  LOG("message: ");
  LOG(" time: %lld", message.time);
  LOG(" voltage: %.3f", message.voltage);
  LOG(" voltage low: %.3f", message.voltage_low);
  LOG(" voltage high: %.3f", message.voltage_high);

  announceIncomingPacket(message, packet.source);
}

static String serialbuffer = String();
void loop() {
  if(upgrading) {
    ArduinoOTA.handle();
    return;
  }

  wifiLoop();
  recordVoltage();
  lowVoltageSleep();

#ifdef ENABLE_POWER_OPTIMIZATIONS
  if(handle_wake_for_dio0_rise) {
    LOG("woken up for DIO0 rise, handling with LoRa library");
    LoRa.handleDio0Rise();
    handle_wake_for_dio0_rise = false;
  }
#endif

  LL2->daemon();

#ifdef ENABLE_POWER_OPTIMIZATIONS
  if(nextRouterPingInterval < time(NULL)) {
    nextRouterPingInterval = time(NULL) + config.router_ping_interval;
    LL2->queueRoutingPacket();
  }
#endif

  struct Packet packet = LL2->readData();
  if(packet.totalLength > HEADER_LENGTH) {
    LOG("packet received:");
    LOG(" rssi: %i", LoRa.packetRssi());
    LOG(" snr: %f dB", LoRa.packetSnr());
    LOG(" packet frequency error: %li Hz", LoRa.packetFrequencyError());
    LOG(" receiver: " PRINTF_ADDRESS_PATTERN, PRINTF_ADDRESS(packet.receiver));
    LOG(" sender: " PRINTF_ADDRESS_PATTERN, PRINTF_ADDRESS(packet.sender));
    LOG(" source: " PRINTF_ADDRESS_PATTERN, PRINTF_ADDRESS(packet.source));
    LOG(" ttl: %u", packet.ttl);
    LOG(" totalLength: %u", packet.totalLength);
    LOG(" sequence: %u", packet.sequence);
    LOG(" hopCount: %u", packet.hopCount);
    LOG(" metric: %u", packet.metric);
    LOG(" datagram:");
    LOG("  destination: " PRINTF_ADDRESS_PATTERN, PRINTF_ADDRESS(packet.datagram.destination));
    LOG("  type: %c", packet.datagram.type);

    switch(packet.datagram.type) {
      case 'z':
        printStatusMessage(packet);
      break;
      case 'u':
        LOG("Received upgrade packet, activating AP");
        reactivateAP();
      break;
      default:
        LOG("Unknown packet type! %c", packet.datagram.type);
      break;
    }
  }


#ifdef SEND_BEACONS
  if(nextBeacon < time(NULL)) {
    nextBeacon = time(NULL) + config.beacon_interval;
    // sendUpgradeMessage();
    sendStatusMessage();
  }
#endif

#ifdef ENABLE_SERIAL_CONSOLE
  if(Serial.available() > 0) {
    char c = Serial.read();
    Serial.printf("%c", c);
    Serial.flush();
    if(c == '\r') {
      Serial.println();
      cliInputCB(serialbuffer, LL2);
      serialbuffer = String();
    } else {
      serialbuffer += String(c);
    }
  }
#endif

#ifdef ENABLE_POWER_OPTIMIZATIONS
  if(wifiEnabled()) {
    return;
  }

#ifdef SEND_BEACONS
  uint64_t wakeup_time = min((nextBeacon-time(NULL)), nextRouterPingInterval-time(NULL));
#else
  uint64_t wakeup_time = nextRouterPingInterval-time(NULL);
#endif

  // don't sleep for less than 1 second
  if(wakeup_time < 1) {
    return;
  }

  if(!Layer1->rxBuffer->empty() || !Layer1->txBuffer->empty()) {
    return;
  }

  esp_sleep_enable_ext0_wakeup((gpio_num_t)LORA_IRQ, RISING);

  LOG("Setting wakeup timer for %llu seconds", wakeup_time);
  esp_sleep_enable_timer_wakeup(wakeup_time*1000000);
  saveNetworkState();
  LOG("Entering deep sleep, have been awake for %lu ms", millis());
#ifdef LOG_TO_SERIAL
  Serial.flush();
#endif
  esp_deep_sleep_start();
#endif
}
