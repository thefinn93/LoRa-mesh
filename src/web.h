#ifndef LORA_MESH_WEB_H
#define LORA_MESH_WEB_H
#include <LoRaLayer2.h>

#include "proto/messages.pb.h"

void setupWebserver();
void announceIncomingPacket(StatusMessage message, uint8_t sender[ADDR_LENGTH]);

#endif
