#!/bin/sh
set -exu

nanopb="$(python3 -c 'import nanopb; print(nanopb.__path__[0])')"
protoc --plugin=protoc-gen-nanopb="${nanopb}/generator/protoc-gen-nanopb" -I"${nanopb}/generator/proto" --nanopb_out=src/proto --proto_path src/proto src/proto/*.proto
